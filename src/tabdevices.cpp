#include "tabdevices.h"
#include "ui_tabdevices.h"


TabDevices::TabDevices(nfc_context *context, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabDevices),
    context(context)
{

    ui->setupUi(this);

    ui->lblVersion->setText(nfc_version());

    this->UpdateDevList();

    ui->listWidget->setViewMode(QListView::ListMode);
    ui->listWidget->setIconSize(QSize(32, 32));
    ui->listWidget->setMovement(QListView::Static);
    ui->listWidget->setMaximumWidth(400);
    ui->listWidget->setSpacing(2);

    connect(ui->btnScan, SIGNAL(clicked(bool)), this, SLOT(UpdateDevList()));

    connect(ui->listWidget, &QListWidget::currentItemChanged, this, &TabDevices::changeDeviceSelected);

    connect(ui->btnExit, &QPushButton::clicked, [=]{
        QApplication::quit();
    });
}

TabDevices::~TabDevices()
{
    delete ui;
}

void TabDevices::UpdateDevList() {

    ScanNFCDevices();

    QMapIterator<int, NFC_Device*> i(devList);

    ui->listWidget->clear();
    ui->lblName->clear();
    ui->lblConnString->clear();
    ui->lblInfo->clear();

    while(i.hasNext()) {
        i.next();
        NFC_Device *dev = i.value();
        QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
        if(dev->connectionString.contains("usb"))
            item->setIcon(QIcon(":/images/usb.png"));
        else if(dev->connectionString.contains("uart"))
            item->setIcon(QIcon(":/images/serial.png"));
        else
            item->setIcon(QIcon(":/images/unknown.png"));

        item->setText(dev->name);
        item->setTextAlignment(Qt::AlignVCenter);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item->setData(Qt::UserRole, i.key());
    }
}

int TabDevices::ScanNFCDevices() {

    devList.clear();
    nfc_connstring connstrings[MAX_DEVICE_COUNT];
    size_t szDeviceFound = nfc_list_devices(context, connstrings, MAX_DEVICE_COUNT);

    for(size_t i = 0; i<szDeviceFound; i++) {
        nfc_device *pnd = nfc_open(context, connstrings[i]);
        if(pnd!=nullptr) {
            NFC_Device *device = new NFC_Device(connstrings[i], pnd);
            devList.insert(i, device);
            nfc_close(pnd);
        }
    }
    return szDeviceFound;
}

void TabDevices::changeDeviceSelected(QListWidgetItem *current, QListWidgetItem *previous)
{
    if (!current)
        current = previous;

    int key = current->data(Qt::UserRole).toUInt();
    NFC_Device *dev = devList.value(key);

    ui->lblName->setText(dev->name);
    ui->lblConnString->setText(dev->connectionString);
    ui->lblInfo->setText(dev->info);
}
