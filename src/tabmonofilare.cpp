#include "tabmonofilare.h"
#include "ui_tabmonofilare.h"

QMutex freeToScan;

TabMonofilare::TabMonofilare(nfc_context *context, TabCsv *tabCsv, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabMonofilare),
    context(context)
{
    ui->setupUi(this);

    this->tabCsv = tabCsv;

    tagsOk = 0;
    tagsKo = 0;
    tagsSkipped = 0;
    tagsAsync = 0;

    //================================================================ LOGGER
    ui->txtLog->setDisabled(true);
    QPalette p =ui->txtLog->palette();
    p.setColor(QPalette::Base, QColor(255,255,255));
    p.setColor(QPalette::Text, QColor(0,0,0));
    ui->txtLog->setPalette(p);

    logger = new Logger(ui->txtLog, QString("Monofilare-%1.txt").arg(QDateTime::currentDateTime().toString("dd.MM.yy-hh:mm:ss")));
    logger->OpenLogFile();

    csv = new CSV();
    csv->OpenCSVFile( QString("Report-%1.txt").arg(QDateTime::currentDateTime().toString("dd.MM.yy-hh:mm:ss")));

    //WRITE HEADER IN CSV
    csv->AddRecord(tabCsv->Headers);
    //=======================================================================

    //============================================================ PHOTOCELLS
    prevTrigger1 = false;
    prevTrigger2 = false;

    Photocell_1 = new GPIOMonitor(4,23, "Photocell 1");
    Photocell_2 = new GPIOMonitor(4,22, "Photocell 2");
    Photocell_1->Enable();
    Photocell_2->Enable();

    connect(Photocell_1, SIGNAL(valueChanged(bool)), this, SLOT(photoCell_1_Trigger(bool)));
    connect(Photocell_2, SIGNAL(valueChanged(bool)), this, SLOT(photoCell_2_Trigger(bool)));

    photoCounter1 = 0;
    photoCounter2 = 0;

    timer1.start();         // Timers to calculate elapsed time between photocells trigger
    timer2.start();
    //=======================================================================

    //==================================================== USB ASYNC NOTIFIER
    m_usb = new UsbNotifier(0,0,this);
    m_usb->addDeviceFilter(NXP_VID, PN533_PID);
    m_usb->addDeviceFilter(SCM_VID, SCL3711_PID);

    connect(m_usb, SIGNAL(deviceAttached(UsbDevice*)), this, SLOT(deviceAttached(UsbDevice*)));
    connect(m_usb, SIGNAL(deviceDetached(int, int)), this, SLOT(deviceDetached(int, int)));

    deviceMap = m_usb->getDeviceList();
    foreach(QString s, deviceMap.keys()) {
        UsbDevice *ud = deviceMap[s];
        logger->Info(QString("Device present: %1").arg(ud->fullAddress()));
        addDeviceToQList(ud);
    }

    currNfcConnString = QString::null;
    m_usb->start();

    //=======================================================================

    //===================================================== GRAPH PLOT WIDGET
    // set dark background :
    ui->customPlot->setBackground(QBrush(QColor(Qt::gray)));

    // create empty bar chart objects:
    barsOk = new QCPBars(ui->customPlot->xAxis, ui->customPlot->yAxis);
    barsOk->setAntialiased(false);
    barsOk->setPen(QPen(QColor(Qt::green).lighter(170)));
    barsOk->setBrush(QColor(Qt::green));
    barsOk->setName("Correct");

    barsKo = new QCPBars(ui->customPlot->xAxis, ui->customPlot->yAxis);
    barsKo->setAntialiased(false);
    barsKo->setPen(QPen(QColor(Qt::red).lighter(170)));
    barsKo->setBrush(QColor(Qt::red));
    barsKo->setName("Error");

    barsSkip = new QCPBars(ui->customPlot->xAxis, ui->customPlot->yAxis);
    barsSkip->setAntialiased(false);
    barsSkip->setPen(QPen(QColor(Qt::yellow).lighter(170)));
    barsSkip->setBrush(QColor(Qt::yellow));
    barsSkip->setName("Skipped");

    barsAsync = new QCPBars(ui->customPlot->xAxis, ui->customPlot->yAxis);
    barsAsync->setAntialiased(false);
    barsAsync->setPen(QPen(QColor(Qt::cyan).lighter(170)));
    barsAsync->setBrush(QColor(Qt::cyan));
    barsAsync->setName("Async");

    // prepare x axis with time labels:
    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%h:%m:%s:%z");
    ui->customPlot->xAxis->setTicker(timeTicker);
    ui->customPlot->xAxis->setVisible(false);

    // prepare y axis:
    ui->customPlot->yAxis->setRange(-2.5, 2.5);
    ui->customPlot->yAxis->setPadding(5); // a bit more space to the left border
    ui->customPlot->yAxis->setVisible(true);
    ui->customPlot->yAxis->setTickLabels(false);
    ui->customPlot->yAxis->setSubTicks(false);

    QCPLayoutGrid *subLayout = new QCPLayoutGrid;
    ui->customPlot->plotLayout()->insertColumn(0);
    ui->customPlot->plotLayout()->addElement(0, 0, subLayout);
    subLayout->addElement(0, 0, new QCPLayoutElement);
    subLayout->addElement(1, 0, ui->customPlot->legend);
    subLayout->addElement(2, 0, new QCPLayoutElement);
    ui->customPlot->plotLayout()->setColumnStretchFactor(0, 0.001);

    ui->customPlot->legend->setIconSize(20,10);
    ui->customPlot->legend->setMargins(QMargins(2,2,2,2));
    ui->customPlot->legend->setFont(QFont(QFont().family(), 8));
    ui->customPlot->legend->setVisible(true);

    ui->customPlot->legend->setBrush(QColor(255, 255, 255, 0));
    ui->customPlot->legend->setBorderPen(Qt::NoPen);

    ui->customPlot->setInteractions(QCP::iRangeDrag);
    ui->customPlot->axisRect()->setRangeDrag(Qt::Horizontal);
    //=======================================================================

    //====================================================== NFC DEVICES LIST
    currNfcDev = nullptr;
    ui->listWidget->setViewMode(QListView::ListMode);
    ui->listWidget->setIconSize(QSize(24, 24));
    ui->listWidget->setMovement(QListView::Static);
    ui->listWidget->setMaximumWidth(400);
    ui->listWidget->setSpacing(2);
    connect(ui->listWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
            this, SLOT(deviceSelected(QListWidgetItem*,QListWidgetItem*)));
    //=======================================================================

    //============================================================ UI BUTTONS
    QState *start = new QState();
    start->setObjectName("start");
    start->assignProperty(ui->btn_Test, "text", "Click to\nStop");
    start->assignProperty(ui->btn_Test, "styleSheet","background-color:rgb(0,255,0);");

    QState *stop = new QState();
    stop->setObjectName("stop");
    stop->assignProperty(ui->btn_Test, "text", "Click to\nStart");
    stop->assignProperty(ui->btn_Test, "styleSheet","background-color:rgb(190,190,190);");

    stop->addTransition(ui->btn_Test, SIGNAL(clicked()), start);
    start->addTransition(this, SIGNAL(testAborted()), stop);
    start->addTransition(ui->btn_Test, SIGNAL(clicked()), stop);

    machine.addState(stop);
    machine.addState(start);

    machine.setInitialState(stop);
    machine.start();

    connect(start, SIGNAL(entered()), this, SLOT(StartTest()));
    connect(stop, SIGNAL(entered()), this, SLOT(StopTest()));

    connect(ui->btn_Reset, &QPushButton::clicked, [=]{
        photoCounter1 = 0;
        photoCounter2 = 0;
        tagsOk = 0;
        tagsKo = 0;
        tagsSkipped = 0;
        tagsAsync = 0;

        ui->lbl_photoCount_1->setText("0");
        ui->lbl_photoCount_2->setText("0");
        ui->lbl_TagOk->setText("0");
        ui->lbl_TagKo->setText("0");
        ui->lbl_TagSkip->setText("0");
        ui->lbl_TagAsync->setText("0");
        QWidget::repaint();
    });
    //=======================================================================
}

TabMonofilare::~TabMonofilare()
{
    delete ui;
}

//============================================= PHOTOCELLS TRIGGERS SLOTS
void TabMonofilare::photoCell_1_Trigger(bool value) {
    // Remove False triggers at startup:
    if(value == prevTrigger1) return;
    prevTrigger1 = value;

    qint64 val = timer1.elapsed();
    timer1.restart();

    if(value == true) {                                         // Photocell went high
        lastDown1 = val;
        emit startBottle();
    } else if(value == false) {                                 // Photocell went down
        lastUp1 = val;
        photoCounter1++;
        emit photoCell_1_CountUp();
        emit endBottle();
        convSpeed = (BOTTLE_NECK_WIDTH/ 1000.0f) / (lastUp1 / 1000.0f); // m/sec
    }

    ui->lbl_emptyTime->setText(QString::number(lastDown1));
    ui->lbl_neckTime->setText(QString::number(lastUp1));
    ui->lbl_photoCount_1->setText(QString::number(photoCounter1));
    ui->lbl_convSpeed->setText(QString::number(convSpeed));
}

void TabMonofilare::photoCell_2_Trigger(bool value) {
    // Remove False triggers at startup:
    if(value == prevTrigger2) return;
    prevTrigger2 = value;

    qint64 val = timer2.elapsed();
    timer2.restart();

    if(value == true) {                     // Photocell went high
        lastDown2 = val;
        emit startLimit();
    } else if(value == false) {             // Photocell went down
        lastUp2 = val;
        photoCounter2++;
        emit photoCell_2_CountUp();
        emit endLimit();
    }
    ui->lbl_emptyTime->setText(QString::number(lastDown1));
    ui->lbl_neckTime->setText(QString::number(lastUp1));
    ui->lbl_photoCount_2->setText(QString::number(photoCounter2));
}
//=======================================================================

//============================================== USB ASYNC NOTIFIER SLOTS
void TabMonofilare::deviceAttached(UsbDevice *dev) {
    logger->Info(QString("Attached new PN533 reader @ %1").arg(dev->fullAddress()));
    qDebug() << dev->busNumber() << " " << dev->portNumber();
    deviceMap.insert(dev->fullAddress(), dev);

    addDeviceToQList(dev);
}

void TabMonofilare::deviceDetached(int busNumber, int deviceAddress) {
    QString fa = UsbDevice::createFullAddress(busNumber, deviceAddress);
    logger->Warning(QString("Removed PN533 reader @ %1").arg(fa));

    deviceMap.remove(fa);
    removeDeviceFromQList(fa);

    // Check if the removed device was actually open and used in the test
    // if yes, stop the test and close gracefully the libnfc pnd

    // Check if thread is running and connection strings match
    if(currNfcDev!=nullptr && currNfcDev->connectionString.contains(fa)) {
        currNfcDev->abort = true;
        emit testAborted();

        QMessageBox msgBox(QMessageBox::Critical,
                           "Reader disconnected!",
                           "NFC Reader was disconnected while in use!\nThe test was stopped.",
                           QMessageBox::Ok , this);
        msgBox.exec();
    }
}
//=======================================================================

//=================================== GUI ELEMENTS UPDATE and INTERACTION
void TabMonofilare::addDeviceToQList(UsbDevice *dev) {
    QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
    item->setIcon(QIcon(":/images/usb.png"));
    item->setText(QString("%1/%2 - %3").arg(dev->manufacturer(), dev->product(), dev->fullAddress()));
    item->setTextAlignment(Qt::AlignVCenter);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    item->setData(Qt::UserRole, dev->fullAddress());
}

void TabMonofilare::removeDeviceFromQList(QString fullAddress) {
    // Search for item in QList with fullAddress
    int a = ui->listWidget->count();
    for(int i=0; i< a; i++) {
        QListWidgetItem *item = ui->listWidget->item(i);
        if(item->data(Qt::UserRole) == fullAddress) {
            delete item;
            return;
        }
    }
}

void TabMonofilare::deviceSelected(QListWidgetItem *curr,QListWidgetItem *prev){
    Q_UNUSED(prev)

    if(curr != nullptr) {
        // ENABLE BUTTONS FOR TEST START
        ui->btn_Test->setEnabled(true);
        QString devAddr = curr->data(Qt::UserRole).toString();
        logger->Info("Selected "+devAddr);

        currNfcConnString = devAddr;

    } else {
        // READER WAS DISCONNECTED WHILE SELECTED
        currNfcConnString = QString::null;
        ui->btn_Test->setEnabled(false);
    }

}
//=======================================================================

//=========================================== CHART PLOT UPDATE PROCEDURE
void TabMonofilare::realtimePlot(QCPBars *bar, int data)
{
    static QTime time(QTime::currentTime());
    double key = time.elapsed()/100;        // Time elapsed in tenth of secs

    bar->addData(key, data);

    // make key axis range scroll right with the data at a constant range of 8
    //ui->customPlot->graph(0)->rescaleValueAxis();
    ui->customPlot->xAxis->setRange(key+1, 100, Qt::AlignRight);
    ui->customPlot->replot();
}
//=======================================================================

void TabMonofilare::StartTest() {
    logger->Info("Started Test");
    emit testStarted();

    //DISABLE CHECKBOX for 2ND PHOTOCELL
    ui->cb_photoCell2->setEnabled(false);

    //CREATE NFC DEVICE REFERENCE
    currNfcDev = new NFC_Device(context);

    //CREATE CONNECTION STRING FROM SELECTED DEVICE LIST
    QString cs = QString("pn53x_usb:%1").arg(currNfcConnString);

    if(currNfcDev->Open(cs) && currNfcDev->InitiatorInit()) {
        logger->Info("NFC device opened");

        // Get additional information if needed
        // qDebug() << currNfcDev->GetDeviceCapabilites();
        // qDebug() << currNfcDev->GetDeviceName();

        connect(this, SIGNAL(startBottle()),this, SLOT(startSingleRead()));
        connect(currNfcDev, SIGNAL(singleReadDone(QString, QString)), this, SLOT(singleReadDone(QString, QString)));
        connect(currNfcDev, SIGNAL(singleReadFailed()), this, SLOT(singleReadFailed()));
        if(ui->cb_photoCell2->isChecked())
            connect(this, SIGNAL(endLimit()), this, SLOT(endSingleRead()));
        else
            connect(this, SIGNAL(endBottle()), this, SLOT(endSingleRead()));

        logger->Info("Started NFC Reader Thread");
        currNfcDev->start();

    } else {
        logger->Error("Unable to open NFC device");
        ui->btn_Test->animateClick();               // Force state change to STOP,
                                                    // emulating a click on the stop button
    }
}

void TabMonofilare::StopTest() {
    logger->Info("Stopped Test");
    emit testEnded();

    // CLOSE NFC DEVICE
    if(currNfcDev != nullptr) {
        disconnect(currNfcDev, SIGNAL(singleReadDone(QString, QString)), this, SLOT(singleReadDone(QString, QString)));
        disconnect(currNfcDev, SIGNAL(singleReadFailed()), this, SLOT(singleReadFailed()));

        if(currNfcDev->isRunning()) {
            currNfcDev->stop = true;
            currNfcDev->AbortCommand();
            while(!currNfcDev->isFinished());
            logger->Info("NFC device closed");
        }
        // Nfc device is closed automatically when thread finishes
        // hence no need to close it manually
        delete currNfcDev;
        currNfcDev = nullptr;

    }

    disconnect(this, SIGNAL(startBottle()),this, SLOT(startSingleRead()));
    if(ui->cb_photoCell2->isChecked()) disconnect(this, SIGNAL(endLimit()), this, SLOT(endSingleRead()));
    else disconnect(this, SIGNAL(endBottle()), this, SLOT(endSingleRead()));

    //ENABLE CHECKBOX for 2ND PHOTOCELL
    ui->cb_photoCell2->setEnabled(true);

}

void TabMonofilare::startSingleRead() {
    // !! Feature requested by Tommy
    if(expectingTag == true) {
        //if here, it means that a new bottle is passing, and the previous was skipped
        emit endSingleRead();
    }

    mutex.lock();
    expectingTag = true;
    mutex.unlock();
}

void TabMonofilare::endSingleRead() {

    mutex.lock();
    expectingTag = false;

    if(!tagReadDone) {
        tagsSkipped++;
        realtimePlot(barsSkip, -2);
        ui->lbl_TagSkip->setText(QString::number(tagsSkipped));

        // Here photoCounter1 has already been updated by the photocell trigger!
        logger->Error(QString("Tag %1 SKIPPED").arg(photoCounter1));
    } else
        tagReadDone = false;
    mutex.unlock();
}

void TabMonofilare::singleReadDone(QString info, QString uid) {  
    mutex.lock();

    tagReadDone = true;
    if(expectingTag == true) {
        // Tag read between photocells
        tagsOk++;
        realtimePlot(barsOk, 2);
        ui->lbl_TagOk->setText(QString::number(tagsOk));
        logger->Info(QString("Tag %1 OK: %2").arg(photoCounter1+1).arg(info));
        QString record = QString(tabCsv->Format).replace("$UID$", uid);
        csv->AddRecord(record);
    } else {
        // Tag read async (out of photocells scope)
        tagsAsync++;
        realtimePlot(barsAsync, 1);
        ui->lbl_TagAsync->setText(QString::number(tagsAsync));
        logger->Info(QString("Async Tag %1 OK: %2").arg(tagsAsync).arg(info));
        QString record = QString(tabCsv->Format).replace("$UID$", uid).append("[A]");
        csv->AddRecord(record);
    }

    mutex.unlock();
}

void TabMonofilare::singleReadFailed() {
    tagsKo++;
    realtimePlot(barsKo, -1);
    ui->lbl_TagKo->setText(QString::number(tagsKo));
    //Here the photocounter is different if using the second photocell or not
    logger->Error(QString("Tag %1 Read ERROR")
        .arg(photoCounter1 + (1 * (ui->cb_photoCell2->isChecked()?0:1))));
}
