#ifndef NFC_DEVICE_H
#define NFC_DEVICE_H

#include <QThread>
#include <QObject>
#include <QString>
#include <QDebug>
#include <QMutex>

#include <nfc/nfc.h>

extern QMutex freeToScan;

#define MAX_CONSECUTIVE_ERRORS  10

class NFC_Device : public QThread
{
    Q_OBJECT
public:
    explicit NFC_Device(nfc_context *context, QObject *parent = nullptr);

    bool Open(QString connString);
    QString GetDeviceCapabilites();
    QString GetDeviceName();
    void Close(void);
    bool InitiatorInit();
    bool AbortCommand(void);
    bool isOpen();

    nfc_device *getRawPND();

    void run() override;

signals:
    void singleReadDone(QString info, QString uid);
    void singleReadFailed();

public slots:

private:
    nfc_context *context;
    nfc_device *pnd;

    int consecutiveErrors;

    const nfc_modulation nmModulations = { .nmt = NMT_ISO14443A, .nbr = NBR_106 };

    QString UIDToString(nfc_target nt);

public:
    QString     connectionString;
    QString     name;
    QString     info;
    QString     lastUid;
    bool        stop;
    bool        abort;

    void ReadUID();


};
#endif // NFC_DEVICE_H
