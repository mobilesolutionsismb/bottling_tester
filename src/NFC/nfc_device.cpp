
#include "nfc_device.h"
#include "tabmonofilare.h"

NFC_Device::NFC_Device(nfc_context *context, QObject *parent) : QThread(parent)
{
    this->context = context;
}

bool NFC_Device::Open(QString connString) {
    // OPEN DEVICE
    this->connectionString = connString;
    this->pnd = nfc_open(context, this->connectionString.toLatin1().data());
    return (pnd!=nullptr);
}

QString NFC_Device::GetDeviceCapabilites() {
    // GET DEVICE CAPABILITIES (chip, supported modulations, etc)
    char *strinfo = NULL;
    QString info = QString::null;

    if(pnd != nullptr) {
        if(nfc_device_get_information_about(pnd, &strinfo) >= 0) {
            info = QString(strinfo);
            nfc_free(strinfo);
        }
    }

    return info;
}

QString NFC_Device::GetDeviceName() {
    QString name = QString::null;
    if(pnd != nullptr) {
        name = QString(nfc_device_get_name(pnd));
    }
    return name;
}

bool NFC_Device::InitiatorInit() {
    // INITIALIZE DEVICE AS INITIATOR (READER)
    if(pnd!= nullptr )
        return (nfc_initiator_init(this->pnd) == 0);

    return false;
}

void NFC_Device::Close() {
    if(this->pnd != nullptr)
        nfc_close(this->pnd);
}

bool NFC_Device::AbortCommand() {
    if(this->pnd!=nullptr) {
        return nfc_abort_command(this->pnd);
    }
    return false;
}

bool NFC_Device::isOpen() {
    return(this->pnd != nullptr);
}


void NFC_Device::run() {
    stop = false;
    abort = false;
    consecutiveErrors = 0;
    while(!stop && !abort) {
        ReadUID();
    }

    if(!abort)
        this->Close();
}

void NFC_Device::ReadUID() {
    nfc_target nt;
    QString tmp;

    freeToScan.lock();
    int res = nfc_initiator_select_passive_target(this->pnd, this->nmModulations, NULL, 0, &nt);

    if(res > 0) {
        char *s;
        str_nfc_target(&s, &nt, false);

        // Sometimes the tag removal does not work really flawlessly, so we check lastUid
        tmp = UIDToString(nt);
        if(tmp != lastUid) {
            lastUid = tmp;
            emit singleReadDone(QString(s), tmp);
            consecutiveErrors = 0;
        }

        // Wait for Tag removal
        while (0 == nfc_initiator_target_is_present(this->pnd, &nt)) {}
        nfc_free(s);
    }
    else if(res == NFC_ETIMEOUT){
        consecutiveErrors++;
        if(consecutiveErrors <= MAX_CONSECUTIVE_ERRORS)
            emit singleReadFailed();
    } else if(res == NFC_EOPABORTED) {
        qDebug() << "Aborted NFC command";
    }
    freeToScan.unlock();
}

QString NFC_Device::UIDToString(nfc_target nt) {
    QByteArray array((char *)nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
    qDebug() << QString(array.toHex());
    return QString(array.toHex());
}

nfc_device *NFC_Device::getRawPND() {
    return this->pnd;
}
