#ifndef TABEEPD_H
#define TABEEPD_H

#include <QWidget>
#include <QStateMachine>
#include <QDebug>

#include "LOG/logger.h"
#include "EEPD/eepd.h"

namespace Ui {
class TabEEPD;
}

class TabEEPD : public QWidget
{
    Q_OBJECT

public:
    explicit TabEEPD(QWidget *parent = 0);
    ~TabEEPD();

    void updateUiInfo(int bottHour);

private:
    Ui::TabEEPD *ui;

    EEPD *eepd;
    Logger *logger;


private slots:
    void onCBO_StateChanged(int);
    void onCBL_StateChanged(int);
    void onBTNI_Pressed();


};

#endif // TABMOTOR_H
