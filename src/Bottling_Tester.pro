#-------------------------------------------------
#
# Project created by QtCreator 2018-06-07T16:39:10
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Bottling_Tester
TEMPLATE = app

LIBS += -lusb-1.0 -lnfc -lfreefare

# install app to /home/root folder on device
target.path = /home/root
INSTALLS += target

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SVN_BASE_DIR = $${PWD}/../
DESTDIR += $$SVN_BASE_DIR/build

DIR_NFC = $${PWD}/NFC/
include($$DIR_NFC/NFC.pri)

DIR_PWM = $${PWD}/PWM/
include($$DIR_PWM/PWM.pri)

DIR_GPIO = $${PWD}/GPIO/
include($$DIR_GPIO/GPIO.pri)

DIR_STEPPER = $${PWD}/STEPPER/
include($$DIR_STEPPER/STEPPER.pri)

DIR_LOG = $${PWD}/LOG/
include($$DIR_LOG/LOG.pri)

DIR_EEPD = $${PWD}/EEPD/
include($$DIR_EEPD/EEPD.pri)

DIR_USB_NOTIFY = $${PWD}/USB_NOTIFY/
include($$DIR_USB_NOTIFY/USB_NOTIFY.pri)

DIR_PLOT = $${PWD}/PLOT/
include($$DIR_PLOT/PLOT.pri)

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    formulae.cpp \
#    tabdevices.cpp \
    tabmotor.cpp \
    tabmonofilare.cpp \
    tabcsv.cpp \
    tabeepd.cpp

HEADERS += \
    mainwindow.h \
    formulae.h \
#    tabdevices.h \
    tabmotor.h \
    tabmonofilare.h \
    tabcsv.h \
    tabeepd.h

FORMS += \
    mainwindow.ui \
#    tabdevices.ui \
    tabmotor.ui \
    tabmonofilare.ui \
    tabcsv.ui \
    tabeepd.ui

RESOURCES += \
    tabdevices.qrc
