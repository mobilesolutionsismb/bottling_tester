#include "formulae.h"

//============================= Calculate rotations/period from bottles occurencies in time
float Formulae::CalculateBottlesPerSecond(int nBot) {
    return nBot / 3600.0f;
}

float Formulae::CalculateFreqFromBottlesPassages(int nBot, int arms) {
    return nBot / (3600.0f * arms);
}

long Formulae::CalculateFreqStepFromBottlePassages(int nBot, int microsteps, int arms) {
    return (200 * microsteps * nBot) / (3600.0f * arms);
}

long Formulae::CalculatePeriodStepFromBottlePassages(int nBot, int microsteps, int arms) {
    return (3600.0f * arms)/ (200 * microsteps * nBot) * 1000000000UL;
}
//==================================================================================

//========================================== Calculate real linear speed on the line
float Formulae::CalculateLinearSpeedFromBottles(int nBot, float dBot) {
    // nBot is bottle number per hour
    // dBot is distance between bottles on the line in meters
    // return value is in m/sec
    return ((nBot * dBot)/3600.0f);
}
//==================================================================================

// Calculate what the radius of the arm should be to compare to linear speed of line
float Formulae::CalculateExpectedRadius(float linSpeed, float freq) {
    return linSpeed / (2 * M_PI * freq);
}
//==================================================================================

/*
long Formulae::CalculatePeriodStepFromBottles(int nBot, float dBot, float radius, int arms, int microsteps) {
    // all in one calculation, return value in ns
    return (( 3600 * radius * 2 * M_PI * arms) / (200 * microsteps * nBot * dBot) * 1000000000UL);
}
*/

/// seconds as "X days, X hours, X minutes, X seconds" string
QString Formulae::secondsToString(qint64 seconds)
{
  const qint64 DAY = 86400;
  qint64 days = seconds / DAY;
  QTime t = QTime(0,0).addSecs(seconds % DAY);
  return QString("%1 days, %2 hours, %3 minutes, %4 seconds")
    .arg(days).arg(t.hour()).arg(t.minute()).arg(t.second());
}
