#ifndef TABCSV_H
#define TABCSV_H

#include <QWidget>
#include <QDebug>

#define ORDER_NUMBER            "$OrderNumber$"
#define COUNTER                 "$Counter$"
#define MANUFACTURER_NAME       "$ManufacturerName$"
#define PLANT_CODE              "$PlantCode$"
#define MACHINE_CODE            "$MachineCode$"
#define BATCH_CODE              "$BatchCode$"
#define PRODUCT_CODE            "$ProductCode$"
#define PRODUCT_DESCRIPTION     "$ProductDescription$"
#define LOTTERY_CODE            "$LotteryCode$"


namespace Ui {
class TabCsv;
}

class TabCsv : public QWidget
{
    Q_OBJECT

public:
    explicit TabCsv(QWidget *parent = 0);
    ~TabCsv();

    QString Format;
    QString Headers;

    QString OrderNumber;
    QString Counter;
    QString ManufacturerName;
    QString PlantCode;
    QString MachineCode;
    QString BatchCode;
    QString ProductCode;
    QString ProductDescription;
    QString LotteryCode;

private:
    Ui::TabCsv *ui;



    void ReadTextEditValues();

private slots:
    void onApply();

public slots:
    void Activate();
    void Deactivate();
};

#endif // TABCSV_H
