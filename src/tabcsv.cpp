#include "tabcsv.h"
#include "ui_tabcsv.h"

TabCsv::TabCsv(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabCsv)
{
    ui->setupUi(this);

    onApply();

    connect(ui->btnApply, SIGNAL(clicked(bool)), this, SLOT(onApply()));

}

TabCsv::~TabCsv()
{
    delete ui;
}

void TabCsv::Activate() {
    ui->wdgAll->setEnabled(true);
}

void TabCsv::Deactivate() {
    ui->wdgAll->setEnabled(false);
}

void TabCsv::ReadTextEditValues() {
    OrderNumber = ui->te_OrderNumber->text();
    Counter = ui->te_Counter->text();
    ManufacturerName = ui->te_Manufacturer->text();
    PlantCode = ui->te_PlantCode->text();
    MachineCode = ui->te_MachineCode->text();
    BatchCode = ui->te_BatchCode->text();
    ProductCode = ui->te_ProdCode->text();
    ProductDescription = ui->te_ProdDesc->text();
    LotteryCode = ui->te_LotteryCode->text();
}

void TabCsv::onApply() {
    ReadTextEditValues();
    Format = ui->te_Format->toPlainText();

    Headers = QString(Format);
    Headers = Headers.replace("$", "");

    Format.replace(ORDER_NUMBER,        OrderNumber);
    Format.replace(COUNTER,             Counter);
    Format.replace(MANUFACTURER_NAME,   ManufacturerName);
    Format.replace(PLANT_CODE,          PlantCode);
    Format.replace(MACHINE_CODE,        MachineCode);
    Format.replace(BATCH_CODE,          BatchCode);
    Format.replace(PRODUCT_CODE,        ProductCode);
    Format.replace(PRODUCT_DESCRIPTION, ProductDescription);
    Format.replace(LOTTERY_CODE,        LotteryCode);
}
