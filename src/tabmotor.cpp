#include "tabmotor.h"
#include "ui_tabmotor.h"

#include <QDebug>
#include <formulae.h>

#define BOTTLE_DISTANCE 0.12f
#define ROTATION_ARM    0.08f
#define NUM_ARMS        4
#define MICROSTEPS      32

TabMotor::TabMotor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabMotor)
{
    ui->setupUi(this);


    ui->txtLog->setDisabled(true);
    QPalette p =ui->txtLog->palette();
    p.setColor(QPalette::Base, QColor(255,255,255));
    p.setColor(QPalette::Text, QColor(0,0,0));
    ui->txtLog->setPalette(p);
    logger = new Logger(ui->txtLog);

    //========================== Stepper Motor GPIOS =============================
    GPIO *en = new GPIO(1, 5, GPIO::ActiveLevel::ACTIVE_LOW, "Enable");
    GPIO *dir = new GPIO(1, 4, GPIO::ActiveLevel::ACTIVE_HIGH, "Direction");
    //dir->On();          //CCW
    dir->Off();         //CW

    PWM_Helper *stp = new PWM_Helper(1, 0, "Step");     // GPIO1_9
    stp->Init();

    motor = new Stepper(en, dir, stp);

    //============================================================================

    //========================== Dial and up down buttons ========================
    connect(ui->dialBott, SIGNAL(valueChanged(int)), this, SLOT(onDialChanged()));
    connect(ui->btnLow,  &QPushButton::clicked, [=]() { ui->dialBott->setValue(ui->dialBott->value() -1 ); });
    connect(ui->btnHigh, &QPushButton::clicked, [=]() { ui->dialBott->setValue(ui->dialBott->value() +1 ); });
    connect(ui->btnLow_2,  &QPushButton::clicked, [=]() { ui->dialBott->setValue(ui->dialBott->value() -10 ); });
    connect(ui->btnHigh_2, &QPushButton::clicked, [=]() { ui->dialBott->setValue(ui->dialBott->value() +10 ); });

    // Set initial value
    ui->dialBott->setValue(ui->dialBott->minimum());

    //============================================================================

    //============================= Various parameters ===========================
    bottle_distance = BOTTLE_DISTANCE;
    num_arms = NUM_ARMS;
    microsteps = MICROSTEPS;
    ui->sb_microsteps->setValue(microsteps);
    ui->sb_arms->setValue(num_arms);
    ui->sb_distance->setValue(bottle_distance);


    ui->rb_cw->setChecked(true);
    connect(ui->rb_cw, QOverload<bool>::of(&QRadioButton::clicked), [=]() {dir->Off();});
    connect(ui->rb_ccw, QOverload<bool>::of(&QRadioButton::clicked), [=]() {dir->On();});

    connect(ui->sb_microsteps, QOverload<int>::of(&QSpinBox::valueChanged), [=](int i) {microsteps = i; });
    connect(ui->sb_arms, QOverload<int>::of(&QSpinBox::valueChanged), [=](int i) {num_arms = i; });
    connect(ui->sb_distance, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double i) {
        bottle_distance = i;
        updateUiInfo(ui->dialBott->value() * 100);
    });
    //============================================================================

    //========================== ON/OFF Motor button =============================
    QState *off = new QState();
    off->assignProperty(ui->btnEnable, "text", "Enable Motor");
    off->setObjectName("off");
    off->assignProperty(ui->btnEnable, "styleSheet","background-color:rgb(190,190,190);");

    QState *on = new QState();
    on->setObjectName("on");
    on->assignProperty(ui->btnEnable, "text", "Disable Motor");
    on->assignProperty(ui->btnEnable, "styleSheet","background-color:rgb(0,255,0);");

    off->addTransition(ui->btnEnable, SIGNAL(clicked()), on);
    on->addTransition(ui->btnEnable, SIGNAL(clicked()), off);

    machine.addState(off);
    machine.addState(on);

    machine.setInitialState(off);
    machine.start();

    connect(on, &QState::entered, [=]() {

        // Disable parameters change
        ui->wdgParams->setEnabled(false);
        ui->wdgRot->setEnabled(false);

        // Enable Motor
        motor->EnableMotor(
            Formulae::CalculatePeriodStepFromBottlePassages(ui->dialBott->value() * 100, microsteps, num_arms)
        );
        logger->Info("Enabling motor with ramping...");
    });



    connect(off, &QState::entered, [=]() {
        // Enable parameters change
        ui->wdgParams->setEnabled(true);
        ui->wdgRot->setEnabled(true);

        // Disable Motor
        motor->DisableMotor();

        logger->Info("Disabling motor");

    });

    //============================================================================

    connect(motor, &Stepper::MotorUpToSpeed, [=](){
        ui->wdgControl->setEnabled(true);
        logger->Info("Ramping done, motor up to speed");
    });
}

TabMotor::~TabMotor()
{
    delete ui;
}


void TabMotor::updateUiInfo(int bottHour)
{
    float linear, freq, step_f, step_T, radius;

    if(bottHour == 0)
        return;

    ui->lblBotHour->setText(QString::number(bottHour));

    freq   = Formulae::CalculateFreqFromBottlesPassages(bottHour, num_arms);
    step_f = Formulae::CalculateFreqStepFromBottlePassages(bottHour, microsteps, num_arms);
    qDebug() << "Freq: "<< step_f;
    step_T = Formulae::CalculatePeriodStepFromBottlePassages(bottHour, microsteps, num_arms);
    ui->lblPeriodNs->setText(QString::number(step_T));

    linear = Formulae::CalculateLinearSpeedFromBottles(bottHour, bottle_distance);
    ui->lblLinear->setText(QString::number(linear));

    radius = Formulae::CalculateExpectedRadius(linear, freq);
    ui->lblRadius->setText(QString::number(radius));
}


void TabMotor::onDialChanged() {
    int bottles = ui->dialBott->value() * 100;

    if(motor->isOn()) {
        ui->wdgControl->setEnabled(false);
        motor->Ramp2Period(Formulae::CalculatePeriodStepFromBottlePassages(bottles, microsteps, num_arms));
        logger->Info("Ramping motor...");
    }

    this->updateUiInfo(bottles);
}
