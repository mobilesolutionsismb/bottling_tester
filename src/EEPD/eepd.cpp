#include "eepd.h"

QMutex mutexRead;

EEPD::EEPD(QString ttyName, QObject *parent) : QObject(parent)
{
    this->ttyName = ttyName;
    QtConcurrent::run( [&]() {
        while(1) {
            mutexRead.lock();
            if(serialDev->isOpen() && serialDev->bytesAvailable())
            {
                QString resp(serialDev->readAll());

                // Parse and split async notification
                QStringList tkn = resp.split(QRegExp(
                    QString("%1%2|%3r")
                        .arg(RESP_START_SEQ)
                        .arg(RESP_OK)
                        .arg(RESP_START_SEQ)
                    ), QString::SkipEmptyParts);

                foreach(QString async, tkn ) {
                    QStringList t2 = async.split('=');
                    emit InputChanged(t2[0].toInt(), t2[1].toInt());
                }
                //qDebug() << "Async Data: " + resp;
            }
            mutexRead.unlock();
            QThread::sleep(10);
        }
    });
}

//-------------------- Device Functions
EEPD::Error EEPD::OpenDevice(QSerialPortInfo *outInfo, QString *outRetVal) {
    serialDev = new QSerialPort();
    serialDev->setPortName(this->ttyName);

    if (!serialDev->open(QIODevice::ReadWrite)) {
        *outRetVal = serialDev->error();

        return OPEN_FAILED;
    }

    QSerialPortInfo i(*serialDev);
    *outInfo = i;
    return OK;
}

EEPD::Error EEPD::CloseDevice() {
    if(serialDev->isOpen()) {
        serialDev->close();
    }
    return OK;
}

//-------------------- Protocol Configuration Functions
EEPD::Error EEPD::ConfigureAllOutputs() {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_CONF)
        .arg(PAR_ALL_OUT)
        .arg(CONF_SET_OUT);

    //------------------------------- BUGFIX on PROTOCOL
    cmd_pl.append(END_SEQ); // this command is buggy...(see protocol.h)
    //-------------------------------

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}

EEPD::Error EEPD::ConfigureAllInputs() {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_CONF)
        .arg(PAR_ALL_IN)
        .arg(CONF_SET_IN);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}

EEPD::Error EEPD::ConfigureLedsAsync() {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_CONF)
        .arg(PAR_ALL_OUT)
        .arg(CONF_LED_NOSYNC);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}

EEPD::Error EEPD::ConfigureLedsSync() {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_CONF)
        .arg(PAR_ALL_OUT)
        .arg(CONF_LED_SYNC);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}

EEPD::Error EEPD::ConfigureAllDebounce(int debounceMs) {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3%4")
        .arg(CMD_CONF)
        .arg(PAR_ALL_IN)
        .arg(CONF_DEBOUNCE)
        .arg(debounceMs, 6, 10, QChar('0'));

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}

EEPD::Error EEPD::ConfigureInputNotification(QString gpiId) {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_CONF)
        .arg(gpiId)
        .arg(CONF_CHG_NOTIF);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}


//-------------------- Protocol Actions Functions
EEPD::Error EEPD::SwitchLED(QString ledId, int on_off) {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_LED)
        .arg(ledId)
        .arg(on_off);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}

EEPD::Error EEPD::SwitchOutput(QString gpoId, int on_off) {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_WRITE)
        .arg(gpoId)
        .arg(on_off);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}

EEPD::Error EEPD::ReadInput(QString gpiId, int *val){
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2")
        .arg(CMD_READ)
        .arg(gpiId);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    e = CheckResponse(cmd_pl, response, &tkns);
    if(e == OK) {
        QString t = tkns.first();
        *val = (t.split(RESP_VALUE).last()).toInt();
    }

    return e;
}

//-------------------- Device Info Functions
EEPD::Error EEPD::ReadSerialNumber(QString *sn) {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2")
        .arg(CMD_READ)
        .arg(PAR_SER_NUM);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    e = CheckResponse(cmd_pl, response, &tkns);
    if(e == OK) {
        QString t = tkns.first();
        *sn = t.split(RESP_VALUE).last();
    }

    return e;
}

EEPD::Error EEPD::ReadSmallBootBlock(QString *sb){
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2")
        .arg(CMD_READ)
        .arg(PAR_FW_SM_BL);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    e = CheckResponse(cmd_pl, response, &tkns);
    if(e == OK) {
        QString t = tkns.first();
        *sb = t.split(RESP_VALUE).last();
    }

    return e;
}

EEPD::Error EEPD::ReadFirmwareLoader(QString *fl){
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2")
        .arg(CMD_READ)
        .arg(PAR_FW_REV);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    e = CheckResponse(cmd_pl, response, &tkns);
    if(e == OK) {
        QString t = tkns.first();
        *fl = t.split(RESP_VALUE).last();
    }

    return e;
}

EEPD::Error EEPD::ReadAdapterNumber(QString *an){
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2")
        .arg(CMD_READ)
        .arg(PAR_ADAPT_NUM);

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    e = CheckResponse(cmd_pl, response, &tkns);
    if(e == OK) {
        QString t = tkns.first();
        *an = t.split(RESP_VALUE).last();
    }

    return e;
}


EEPD::Error EEPD::TestError() {
    EEPD::Error e;
    QString cmd,cmd_pl, response;
    QStringList tkns;

    cmd_pl = QString("%1%2%3")
        .arg(CMD_CONF)
        .arg(GPI9)
        .arg("d");

    cmd = QString("%1%2%3")
        .arg(START_SEQ)
        .arg(cmd_pl)
        .arg(END_SEQ);

    e = SendCmd(cmd, &response);
    if(e != OK)
        return e;

    return CheckResponse(cmd_pl, response, &tkns);
}


//-------------------- Private Functions
EEPD::Error EEPD::SendCmd(QString cmd, QString *response, int timeout) {
    if(serialDev->isOpen()) {
        // write request

        QByteArray requestData = cmd.toLocal8Bit();
        serialDev->write(requestData);
        if (serialDev->waitForBytesWritten(wait_timeout)) {
            // read response
            mutexRead.lock();
            if (serialDev->waitForReadyRead(wait_timeout)) {
                QByteArray responseData = serialDev->readAll();
                while (serialDev->waitForReadyRead(timeout))
                    responseData += serialDev->readAll();

                QString resp(responseData);
                *response = resp;
                qDebug() << *response;
                mutexRead.unlock();
                return OK;
            } else {
                mutexRead.unlock();
                return READ_TIMEOUT;
            }
        } else
            return WRITE_TIMEOUT;
    } else
        return OPEN_FAILED;
}

EEPD::Error EEPD::CheckResponse(QString cmd_pl, QString response, QStringList *respTokens) {
    //QString rxp = QString("%1|%2").arg(RESP_START_SEQ).arg(RESP_VALUE);
    Error e = UNKNOWN_ERROR;

    QString rxp = QString("%1").arg(RESP_START_SEQ);

    *respTokens = response.split(QRegExp(rxp),QString::SkipEmptyParts);

    // Check tokens number
    if(respTokens->length() < 2 || respTokens->isEmpty())
        return UNKNOWN_ERROR;

    // Check response status (Last Token)
    QString rs = respTokens->last();
    if(rs == RESP_NVC)
        return Error::NO_VALID_COMMAND;

    if(rs == RESP_ERR)
        return Error::PROTOCOL_ERROR;

    if(rs == RESP_OK)
        e = OK;

    // One before Last Token is command echo
    if(!(respTokens->at(respTokens->size()-2)).contains(cmd_pl))
        return Error::UNKNOWN_ERROR;

    return e;
}
