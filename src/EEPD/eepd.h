#ifndef EEPD_H
#define EEPD_H

#include <QObject>
#include <QDebug>

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QtConcurrent/QtConcurrent>

#include "protocol.h"

#define wait_timeout 10

class EEPD : public QObject
{
    Q_OBJECT
public:
    enum Error{OK, OPEN_FAILED, CLOSE_FAILED, WRITE_TIMEOUT, READ_TIMEOUT,
               NO_VALID_COMMAND, PROTOCOL_ERROR,
               UNKNOWN_ERROR};
        Q_ENUM(Error)

    explicit EEPD(QString ttyName, QObject *parent = nullptr);

    EEPD::Error OpenDevice(QSerialPortInfo *outInfo, QString *outRetVal);
    EEPD::Error CloseDevice();

    EEPD::Error ConfigureAllOutputs();
    EEPD::Error ConfigureAllInputs();
    EEPD::Error ConfigureLedsAsync();
    EEPD::Error ConfigureLedsSync();
    EEPD::Error ConfigureAllDebounce(int debounceMs);
    EEPD::Error ConfigureInputNotification(QString gpiId);

    EEPD::Error SwitchLED(QString ledId, int on_off);
    EEPD::Error SwitchOutput(QString gpoId, int on_off);
    EEPD::Error ReadInput(QString gpiId, int *val);

    EEPD::Error TestError();

    EEPD::Error ReadSerialNumber(QString *sn);
    EEPD::Error ReadSmallBootBlock(QString *sb);
    EEPD::Error ReadFirmwareLoader(QString *fl);
    EEPD::Error ReadAdapterNumber(QString *an);

private:
    QSerialPort *serialDev;
    QString ttyName;

    EEPD::Error SendCmd(QString cmd, QString *response, int timeout = 5);
    EEPD::Error CheckResponse(QString cmd_pl, QString response, QStringList *respTokens);

signals:
    void InputChanged(int gpiId, int value);

public slots:
};

#endif // EEPD_H
