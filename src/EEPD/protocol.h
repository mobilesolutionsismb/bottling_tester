#ifndef PROTOCOL_H
#define PROTOCOL_H


#define START_SEQ       "#"
#define CMD_CONF        "c"
#define CMD_WRITE       "w"
#define CMD_READ        "r"
#define CMD_LED         "l"
#define END_SEQ         "!"
// Some commands needs 2 '!' to end command
//#define END_SEQ_BUG     "!!"        // BUG in DEVICE!!


#define PAR_ALL_IN      "hb"
#define PAR_ALL_OUT     "lb"
#define PAR_ADAPT_NUM   "an"        //WARNING! Use at your own risk when writing...

#define PAR_SER_NUM     "sn"
#define PAR_FW_SM_BL    "sb"
#define PAR_FW_REV      "fl"

#define CONF_SET_OUT    "o"
#define CONF_SET_IN     "i"
#define CONF_LED_NOSYNC "n"
#define CONF_LED_SYNC   "s"
#define CONF_CHG_NOTIF  "cn"
#define CONF_DEBOUNCE   "db"

#define GPO1            "01"
#define GPO2            "02"
#define GPO3            "03"
#define GPO4            "04"
#define GPO5            "05"
#define GPO6            "06"
#define GPO7            "07"
#define GPO8            "08"

#define LED1            GPO1
#define LED2            GPO2
#define LED3            GPO3
#define LED4            GPO4
#define LED5            GPO5
#define LED6            GPO6
#define LED7            GPO7
#define LED8            GPO8

#define GPI9             "09"
#define GPI10            "10"
#define GPI11            "11"
#define GPI12            "12"
#define GPI13            "13"
#define GPI14            "14"
#define GPI15            "15"
#define GPI16            "16"

#define RESP_START_SEQ   "-"
#define RESP_OK          "OK"
#define RESP_ERR         "ER"
#define RESP_NVC         "??"  // No Valid Command
#define RESP_VALUE       "="

#endif // PROTOCOL_H
