#ifndef PWM_HELPER_H
#define PWM_HELPER_H

#include <QObject>
#include <QFile>

#define DUTY_CYCLE_NS 3000
#define PERIOD_VALUE_IN_STOP    5000000

class PWM_Helper : public QObject
{
    Q_OBJECT
public:
    explicit PWM_Helper(int pwmChip, int pwmNum, QString name, QObject *parent = nullptr);

    void Init();
    void Enable(bool on_off);

    void SetPeriodNs(long ns);
    void SetDutyCycleNs(long ns);

    // lower level info (used in sysclass files)
    long dutyCycleNs, periodNs;

private:
    QFile *Open(QString s);
    QString SysFsRoot;

    int pwmChip, pwmNum;
    QString name;



signals:

public slots:
};

#endif // PWM_HELPER_H
