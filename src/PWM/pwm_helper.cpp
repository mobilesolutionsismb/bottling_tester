#include "pwm_helper.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QtMath>

PWM_Helper::PWM_Helper(int pwmChip, int pwmNum, QString name, QObject *parent) : QObject(parent)
{
    this->pwmChip = pwmChip;
    this->pwmNum = pwmNum;
    this->name = name;

    this->SysFsRoot = QString("/sys/class/pwm/pwmchip%1/pwm%2/").arg(pwmChip).arg(pwmNum);
}


void PWM_Helper::Init() {
    this->Enable(false);
    this->SetPeriodNs(PERIOD_VALUE_IN_STOP);
    this->SetDutyCycleNs(DUTY_CYCLE_NS);
    this->Enable(true);
}

QFile *PWM_Helper::Open(QString s) {
    QFile *file = new QFile(SysFsRoot + s);
    if(!file->open(QIODevice::WriteOnly | QIODevice::Text)) {
        return nullptr;
    } else return file;
}

void PWM_Helper::Enable(bool on_off) {
    QFile *f = Open("enable");
    if(f != nullptr) {
        QTextStream out(f);
        out << QString::number(on_off?1:0);
        f->close();
    }
}

void PWM_Helper::SetPeriodNs(long ns) {
    QFile *f = Open("period");

    if(f != nullptr) {
        QTextStream out(f);
        out << QString::number(ns);
        this->periodNs = ns;
        f->close();
    } else {
        qDebug() << "Error setting period";
    }
}

void PWM_Helper::SetDutyCycleNs(long ns) {
    QFile *f = Open("duty_cycle");
    if(f != nullptr) {
        QTextStream out(f);
        out << QString::number(ns );
        this->dutyCycleNs = ns;
        f->close();
    } else {
        qDebug() << "Error setting duty cycle NS";
    }
}
