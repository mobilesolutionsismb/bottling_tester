#include "gpio_monitor.h"

GPIOMonitor::GPIOMonitor(int bank, int pin, QString name, QObject *parent) : QObject(parent)
{
    this->pin = pin;
    this->bank = bank>=1?bank:1;
    this->name = name;

    this->SysFsValue = QString("/sys/class/gpio/gpio%1/value").arg(((bank -1 ) * 32)+pin);
    this->SysFsFile = NULL;
    this->EdgeFsValue = QString("/sys/class/gpio/gpio%1/edge").arg(((bank-1) * 32)+pin);
    this->EdgeFsFile = NULL;
}

bool GPIOMonitor::Open() {
    QFile *valueFile = new QFile(SysFsValue);
    QFile *edgeFile = new QFile(EdgeFsValue);

    if (!valueFile->open(QIODevice::ReadWrite | QIODevice::Text) || !edgeFile->open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "error in open gpio";
        return false;
    }

    this->SysFsFile = valueFile;
    this->EdgeFsFile = edgeFile;
    return true;
}

bool GPIOMonitor::Enable(EdgeInterrupt edgeInterrupt) {
    bool e = this->Open();
    if(e == true) {
        QTextStream out(this->EdgeFsFile);
        QString ss = qVariantFromValue(edgeInterrupt).toString().toLower();
        out << ss;

        m_notifier = new QSocketNotifier(SysFsFile->handle(), QSocketNotifier::Exception);
        connect(m_notifier, &QSocketNotifier::activated, this, &GPIOMonitor::readyReady);

        m_notifier->setEnabled(true);
        this->EdgeFsFile->close();

        return true;
    }
    qDebug() << "error in edge interrupt enable";
    return e;
}

void GPIOMonitor::Disable() {
    delete m_notifier;
    m_notifier = 0;

    this->SysFsFile->close();
}

bool GPIOMonitor::isRunning() const {
    if(!m_notifier) return false;

    return m_notifier->isEnabled();
}

bool GPIOMonitor::Value() const
{
    return m_currentValue;
}

void GPIOMonitor::readyReady(const int &ready)
{
    Q_UNUSED(ready)

    this->SysFsFile->seek(0);
    QByteArray data = this->SysFsFile->readAll();

    bool value = false;
    if (data[0] == '1') {
        value = true;
    } else if (data[0] == '0') {
        value = false;
    } else {
        return;
    }

    m_currentValue = value;
    emit valueChanged(value);
    //qDebug() << "Value Changed on "<< this->name << ": " << value;
}
