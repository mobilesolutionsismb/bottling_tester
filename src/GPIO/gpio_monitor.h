#ifndef GPIO_MONITOR_H
#define GPIO_MONITOR_H

#include <QObject>
#include <QDebug>
#include <QSocketNotifier>
#include <QFile>
#include "gpio_helper.h"

class GPIOMonitor : public QObject
{
    Q_OBJECT
public:
    enum EdgeInterrupt {NONE, RISING, FALLING, BOTH};
    Q_ENUM(EdgeInterrupt)

    explicit GPIOMonitor(int bank, int pin, QString name, QObject *parent = nullptr);

    bool Open();
    bool Enable(EdgeInterrupt edgeInterrupt = EdgeInterrupt::BOTH);
    void Disable();

    bool isRunning() const;
    bool Value() const;

private:
    QString SysFsValue;
    QFile *SysFsFile;
    QString EdgeFsValue;
    QFile *EdgeFsFile;

    int bank;
    int pin;
    EdgeInterrupt edgeInterrupt;
    QString name;

    QSocketNotifier *m_notifier;

    bool m_currentValue;

signals:
    void valueChanged(const bool &value);

public slots:
    void readyReady(const int &ready);
};

#endif // GPIO_MONITOR_H
