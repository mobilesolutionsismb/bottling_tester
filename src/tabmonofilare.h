#ifndef TABMONOFILARE_H
#define TABMONOFILARE_H

#include <QMessageBox>
#include <QWidget>
#include <QSignalMapper>
#include <QElapsedTimer>
#include <QTimer>
#include <QListWidgetItem>
#include <QState>
#include <QStateMachine>
#include <QMutex>
#include <qcustomplot.h>
#include <nfc/nfc.h>

#include <tabcsv.h>

#include "NFC/nfc_device.h"
#include "GPIO/gpio_monitor.h"
#include "LOG/logger.h"
#include "LOG/csv.h"
#include "USB_NOTIFY/usbnotifier.h"
#include "USB_NOTIFY/usbdevice.h"

#define BOTTLE_NECK_WIDTH   31 //mm

#define MAX_DEVICE_COUNT    16
#define MAX_TARGET_COUNT    16

#define MAX_CONSECUTIVE_ERR 10

#define NXP_VID     0x04CC
#define PN533_PID   0x2533

#define SCM_VID     0x04E6
#define SCL3711_PID 0x5591

namespace Ui {
class TabMonofilare;
}

class TabMonofilare : public QWidget
{
    Q_OBJECT

public:
    explicit TabMonofilare(nfc_context *context, TabCsv *tabCsv, QWidget *parent = 0);
    ~TabMonofilare();

private:
    Ui::TabMonofilare *ui;

    GPIOMonitor *Photocell_1;
    GPIOMonitor *Photocell_2;

    QElapsedTimer timer1, timer2;
    qint64 photoCounter1, photoCounter2;

    float convSpeed;
    bool prevTrigger1, prevTrigger2;
    qint64 lastUp1, lastDown1, lastUp2, lastDown2;

    UsbNotifier *m_usb;
    QMap<QString, UsbDevice *> deviceMap;
    QString currNfcConnString;

    nfc_context *context;
    NFC_Device *currNfcDev;

    QStateMachine machine;
    Logger *logger;
    CSV *csv;

    qint64 tagsOk, tagsKo, tagsSkipped, tagsAsync;
    TabCsv *tabCsv;

    QMutex mutex;
    bool expectingTag = false;
    bool tagReadDone = false;

    QTimer timer_plot;
    QCPBars *barsOk, *barsKo, *barsSkip, *barsAsync;

signals:
    void photoCell_1_CountUp();
    void photoCell_2_CountUp();

    // Signals from Photocell 1 to detect bottle
    void startBottle();
    void endBottle();

    // Signals from Photocell 2 to detect NFC Antenna limits
    void startLimit();
    void endLimit();

    void testAborted();
    void testStarted();
    void testEnded();

private slots:
    //------------------------------------------------ GUI ELEMENTS
    void addDeviceToQList(UsbDevice *dev);
    void removeDeviceFromQList(QString fullAddress);
    void deviceSelected(QListWidgetItem *curr,QListWidgetItem *prev);

    void startSingleRead();
    void endSingleRead();

    void singleReadDone(QString info, QString uid);
    void singleReadFailed();

    void realtimePlot(QCPBars *bar, int i);

public slots:
    void photoCell_1_Trigger(bool value);
    void photoCell_2_Trigger(bool value);

    void deviceAttached(UsbDevice *dev);
    void deviceDetached(int busNumber, int deviceAddress);

    void StartTest();
    void StopTest();
};

#endif // TABMONOFILARE_H
