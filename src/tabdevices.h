#ifndef TABDEVICES_H
#define TABDEVICES_H

#include <QWidget>
#include <QListWidget>
#include <QDebug>
#include <QSignalMapper>
#include <nfc/nfc.h>

#include "NFC/nfc_device.h"

#define MAX_DEVICE_COUNT 16
#define MAX_TARGET_COUNT 16

namespace Ui {
class TabDevices;
}

class TabDevices : public QWidget
{
    Q_OBJECT

public:
    explicit TabDevices(nfc_context *context, QWidget *parent = 0);
    ~TabDevices();

private:
    Ui::TabDevices *ui;
    QMap<int, NFC_Device *>     devList;
    nfc_context *context;

    int  ScanNFCDevices();

private slots:
    void UpdateDevList();
    void changeDeviceSelected(QListWidgetItem *current, QListWidgetItem *previous);

};

#endif // TABDEVICES_H
