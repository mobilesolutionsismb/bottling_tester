#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //======================== INIT NFC CONTEXT, ONCE AND FOR ALL
    nfc_context *context;
    nfc_init(&context);

    if(context == NULL)
    {
        QMessageBox msgBox;
        msgBox.setText("Error initializing NFC context!");
        msgBox.setInformativeText("Try rebooting the app...");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    } else {

        TabCsv *tabCsv = new TabCsv();
        TabMonofilare *tabMonofilare = new TabMonofilare(context, tabCsv);

        connect(tabMonofilare, SIGNAL(testStarted()), tabCsv, SLOT(Deactivate()));
        connect(tabMonofilare, SIGNAL(testEnded()), tabCsv, SLOT(Activate()));

        ui->tabWidget->addTab(new TabMotor(), "Stepper Motor" );
        ui->tabWidget->addTab(tabMonofilare, " Test Monofilare");

        ui->tabWidget->addTab(tabCsv, "CSV Structure");
        //ui->tabWidget->addTab(new TabDevices(context),"Devices Infos");

        ui->tabWidget->addTab(new TabEEPD(), "Test EEPD");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
