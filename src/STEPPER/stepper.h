#ifndef STEPPER_H
#define STEPPER_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <cmath>
#include "formulae.h"
#include "GPIO/gpio_helper.h"
#include "PWM/pwm_helper.h"



class Stepper : public QObject
{
    Q_OBJECT
public:
    explicit Stepper(GPIO *en, GPIO *dir, PWM_Helper *stp, QObject *parent = nullptr);

    void EnableMotor(long ns);
    void DisableMotor();
    void Ramp2Period(long targetPeriod);
    bool isOn();

private:
    GPIO *Enable;
    GPIO *Direction;
    PWM_Helper *Step;

signals:
    void periodReached(long ns);
    void MotorStopped();
    void MotorUpToSpeed();

private slots:
    void handlePeriodReached(long ns);

public:
    bool _isOn;
    bool ramping;

private:
};

#endif // STEPPER_H
