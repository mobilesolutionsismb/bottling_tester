#include "stepper.h"

Stepper::Stepper(GPIO *en, GPIO *dir, PWM_Helper *stp, QObject *parent) : QObject(parent),
    Enable(en),
    Direction(dir),
    Step(stp)
{
    ramping = false;
    _isOn = false;

    connect(this, SIGNAL(periodReached(long)), this, SLOT(handlePeriodReached(long)));
}

void Stepper::EnableMotor(long ns) {
    Enable->On();
    _isOn = true;
    Ramp2Period(ns);

}
void Stepper::DisableMotor() {
    _isOn = false;
    Enable->Off();
    //Ramp2Period(PERIOD_VALUE_IN_STOP);
    Step->SetPeriodNs(PERIOD_VALUE_IN_STOP);
}

bool Stepper::isOn() {return _isOn;}

void Stepper::Ramp2Period(long targetPeriod) {
    if(ramping)
        return; //Avoid ramping changes if already ramping


    ramping = true;
    long startPeriod = Step->periodNs;
    long delta = targetPeriod - startPeriod;

    int steps = delta/50;
    double ticks = 100;


    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, [=]()mutable{

        if((steps < 0 && delta - steps <= 0) || (steps > 0 && delta - steps >= 0)){
            delta = delta - steps;
        } else {
            steps = delta;
        }

        Step->SetPeriodNs(Step->periodNs + steps);

        if((steps >= 0 && Step->periodNs >= targetPeriod) || (steps <= 0 && Step->periodNs <= targetPeriod)) {
            timer->stop();
            ramping = false;
            emit(periodReached(targetPeriod));
        }
    });
    timer->start(ticks);
}


//===================== PRIVATE SLOTS
void Stepper::handlePeriodReached(long ns) {
    if(ns == PERIOD_VALUE_IN_STOP) {
        Enable->Off();
        emit MotorStopped();
    } else {
        emit MotorUpToSpeed();
    }

}
