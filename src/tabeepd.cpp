#include "tabeepd.h"
#include "ui_tabeepd.h"

TabEEPD::TabEEPD(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabEEPD)
{
    ui->setupUi(this);


    ui->txtLog->setDisabled(true);
    QPalette p =ui->txtLog->palette();
    p.setColor(QPalette::Base, QColor(255,255,255));
    p.setColor(QPalette::Text, QColor(0,0,0));
    ui->txtLog->setPalette(p);
    logger = new Logger(ui->txtLog);


    QSerialPortInfo qspInfo; QString outRetVal;
    EEPD::Error e;
    eepd = new EEPD("/dev/ttyACM0");
    e = eepd->OpenDevice(&qspInfo, &outRetVal);

    logger->Info("EEPD Device opened: " + qVariantFromValue(e).toString());


    //=================================== Output Controls  ================================
    connect(ui->btnOutputs,  &QPushButton::clicked, [=]() {
        EEPD::Error e = eepd->ConfigureAllOutputs();
        logger->Info("GPO1-8 Configure as outputs: " + qVariantFromValue(e).toString());
    });

    connect(ui->cbo1, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    connect(ui->cbo2, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    connect(ui->cbo3, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    connect(ui->cbo4, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    connect(ui->cbo5, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    connect(ui->cbo6, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    connect(ui->cbo7, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    connect(ui->cbo8, SIGNAL(stateChanged(int)), this, SLOT(onCBO_StateChanged(int)));
    //=====================================================================================


    //=================================== LED Controls  ===================================
    connect(ui->btnAsync, &QPushButton::clicked, [=]() {
        EEPD::Error e = eepd->ConfigureLedsAsync();
        logger->Info("LED1-8 Configure as non-I/O sync: " + qVariantFromValue(e).toString());
    });
    connect(ui->btnSync, &QPushButton::clicked, [=]() {
        EEPD::Error e = eepd->ConfigureLedsSync();
        logger->Info("LED1-8 Configure as I/O sync: " + qVariantFromValue(e).toString());
    });

    connect(ui->cbl1, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    connect(ui->cbl2, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    connect(ui->cbl3, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    connect(ui->cbl4, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    connect(ui->cbl5, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    connect(ui->cbl6, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    connect(ui->cbl7, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    connect(ui->cbl8, SIGNAL(stateChanged(int)), this, SLOT(onCBL_StateChanged(int)));
    //=====================================================================================


    //=================================== Input Controls  =================================
    connect(ui->btnInputs, &QPushButton::clicked, [=]() {
        EEPD::Error e = eepd->ConfigureAllInputs();
        logger->Info("GPI9-16 Configure as inputs: " + qVariantFromValue(e).toString());
    });
    connect(ui->btnDebounce, &QPushButton::clicked, [=]() {
        EEPD::Error e = eepd->ConfigureAllDebounce(10);
        logger->Info("GPI9-16 Configure Debounce: " + qVariantFromValue(e).toString());
    });

    connect(ui->btni9, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    connect(ui->btni10, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    connect(ui->btni11, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    connect(ui->btni12, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    connect(ui->btni13, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    connect(ui->btni14, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    connect(ui->btni15, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    connect(ui->btni16, SIGNAL(clicked(bool)), this, SLOT(onBTNI_Pressed()));
    //=====================================================================================


    //============================== Async Input Controls  ================================
    connect(ui->btn11, &QPushButton::clicked, [=]() {
        EEPD::Error e = eepd->ConfigureInputNotification(PAR_ALL_IN);
        logger->Info("GPI9-16 Configure Notification: " + qVariantFromValue(e).toString());
    });

    connect(eepd, &EEPD::InputChanged, [=](int id, int v) {
        QWidget *qw;
        switch(id) {
            case 9: qw = ui->i1; break;
            case 10: qw = ui->i2; break;
            case 11: qw = ui->i3; break;
            case 12: qw = ui->i4; break;
            case 13: qw = ui->i5; break;
            case 14: qw = ui->i6; break;
            case 15: qw = ui->i7; break;
            case 16: qw = ui->i8; break;
            default: return;
        }

        if(v == 1)
            qw->setStyleSheet("background-color: green");
        else qw->setStyleSheet("background-color: rgb(189, 189, 189)");
    });
    //=====================================================================================


    //============================== Device Info Controls  ================================
    connect(ui->btnWrong, &QPushButton::clicked, [=]() {
        EEPD::Error e = eepd->TestError();
        logger->Info("Test Wrong Command: " + qVariantFromValue(e).toString());
    });

    connect(ui->btnSN, &QPushButton::clicked, [=]() {
        QString sn;
        EEPD::Error e = eepd->ReadSerialNumber(&sn);
        logger->Info("Serial Number: " +
            (e==EEPD::Error::OK?sn:qVariantFromValue(e).toString()));
    });
    connect(ui->btnSB, &QPushButton::clicked, [=]() {
        QString sb;
        EEPD::Error e = eepd->ReadSmallBootBlock(&sb);
        logger->Info("Small Boot Block: " +
            (e==EEPD::Error::OK?sb:qVariantFromValue(e).toString()));
    });
    connect(ui->btnFL, &QPushButton::clicked, [=]() {
        QString fl;
        EEPD::Error e = eepd->ReadFirmwareLoader(&fl);
        logger->Info("Firmware Loader: " +
            (e==EEPD::Error::OK?fl:qVariantFromValue(e).toString()));
    });
    connect(ui->btnAN, &QPushButton::clicked, [=]() {
        QString an;
        EEPD::Error e = eepd->ReadAdapterNumber(&an);
        logger->Info("Adapter Number: " +
            (e==EEPD::Error::OK?an:qVariantFromValue(e).toString()));
    });
    //============================================================================
}

TabEEPD::~TabEEPD()
{
    delete ui;
}

//========================== Private Slots =============================
void TabEEPD::onCBO_StateChanged(int status) {
    QString gpoId;
    QObject *obj = sender();
    int val = status == 0? 0:1;

    if(obj == ui->cbo1) gpoId = GPO1;
    else if(obj == ui->cbo2) gpoId = GPO2;
    else if(obj == ui->cbo3) gpoId = GPO3;
    else if(obj == ui->cbo4) gpoId = GPO4;
    else if(obj == ui->cbo5) gpoId = GPO5;
    else if(obj == ui->cbo6) gpoId = GPO6;
    else if(obj == ui->cbo7) gpoId = GPO7;
    else if(obj == ui->cbo8) gpoId = GPO8;
    else return;

    EEPD::Error e = eepd->SwitchOutput(gpoId, val);
    logger->Info("Output " + gpoId + " to " + QString("%1").arg(val) +": " + qVariantFromValue(e).toString());
}

void TabEEPD::onCBL_StateChanged(int status) {
    QString ledId;
    QObject *obj = sender();
    int val = status == 0? 0:1;

    if(obj == ui->cbl1) ledId = LED1;
    else if(obj == ui->cbl2) ledId = LED2;
    else if(obj == ui->cbl3) ledId = LED3;
    else if(obj == ui->cbl4) ledId = LED4;
    else if(obj == ui->cbl5) ledId = LED5;
    else if(obj == ui->cbl6) ledId = LED6;
    else if(obj == ui->cbl7) ledId = LED7;
    else if(obj == ui->cbl8) ledId = LED8;
    else return;

    EEPD::Error e = eepd->SwitchLED(ledId, val);
    logger->Info("LED " + ledId + " to " + QString("%1").arg(val) +": " + qVariantFromValue(e).toString());
}

void TabEEPD::onBTNI_Pressed() {
    QString gpiId;
    QObject *obj = sender();
    int val;

    if(obj == ui->btni9) gpiId = GPI9;
    else if(obj == ui->btni10) gpiId = GPI10;
    else if(obj == ui->btni11) gpiId = GPI11;
    else if(obj == ui->btni12) gpiId = GPI12;
    else if(obj == ui->btni13) gpiId = GPI13;
    else if(obj == ui->btni14) gpiId = GPI14;
    else if(obj == ui->btni15) gpiId = GPI15;
    else if(obj == ui->btni16) gpiId = GPI16;

    else return;

    EEPD::Error e = eepd->ReadInput(gpiId, &val);
    logger->Info("Read Input " + gpiId + ": " +
        (e==EEPD::Error::OK?QString("%1").arg(val):qVariantFromValue(e).toString()));
}
