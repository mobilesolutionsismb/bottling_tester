#ifndef TABMOTOR_H
#define TABMOTOR_H

#include <QWidget>
#include <QStateMachine>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include "GPIO/gpio_helper.h"
#include "PWM/pwm_helper.h"
#include "STEPPER/stepper.h"
#include "LOG/logger.h"

namespace Ui {
class TabMotor;
}

class TabMotor : public QWidget
{
    Q_OBJECT

public:
    explicit TabMotor(QWidget *parent = 0);
    ~TabMotor();

    void updateUiInfo(int bottHour);

private:
    Ui::TabMotor *ui;
    Stepper *motor;

    QStateMachine machine;

    Logger *logger;

    float bottle_distance;
    int num_arms;
    int microsteps;

public slots:

    void onDialChanged(void);

};

#endif // TABMOTOR_H
