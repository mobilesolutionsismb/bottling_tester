#include "csv.h"

CSV::CSV(QObject *parent) : QObject(parent)
{

}

bool CSV::OpenCSVFile(QString csvFilePath) {
    if(csvFilePath == NULL)
        return false;

    QFile *file = new QFile(csvFilePath);

    if(file->open(QIODevice::ReadWrite | QIODevice::Text)) {
        csvStream = new QTextStream(file);
        csvFilePath = csvFilePath;
        return true;
    } else {
        return false;
    }
}

void CSV::AddRecord(QString s1) {
    if(csvStream != NULL)
        *csvStream << s1 << "\n" << flush;
}

int CSV::CopyToFolder(QString copyToPath) {
    if(csvFilePath != NULL) {
        QFile file(csvFilePath);
        return file.copy(copyToPath);
    } else return -1;
}
