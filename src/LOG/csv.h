#ifndef CSV_H
#define CSV_H

#include <QObject>
#include <QFile>
#include <QTextStream>

class CSV : public QObject
{
    Q_OBJECT
public:
    explicit CSV(QObject *parent = nullptr);
    bool OpenCSVFile(QString csvFilePath);
    void AddRecord(QString s1);
    int CopyToFolder(QString path);

signals:

public slots:

private:
    QTextStream *csvStream;
    QString csvFilePath;
};

#endif // CSV_H
