#include "logger.h"
#include <QTextEdit>
#include <QLabel>
#include <QFile>

#define DATE_FORMAT "hh:mm:ss.zzz"

Logger::Logger(QTextEdit *txtLog, QString logFilePath, QObject *parent) : QObject(parent)
{
    this->logStream = NULL;
    this->logWidget = txtLog;
    if(logFilePath != NULL)
        this->logFilePath = logFilePath;

    connect(this, SIGNAL(scroll()), this, SLOT(scrollToCaret()));
}

bool Logger::OpenLogFile() {

    if(logFilePath == NULL)
        return true;

    QFile *file = new QFile(logFilePath);

    if(file->open(QIODevice::ReadWrite | QIODevice::Text)) {
        logStream = new QTextStream(file);
        return true;
    } else {
        return false;
    }
}

void Logger::Info(QString s) {
    QString s1 = QString("[I] {%1} %2\n").arg(QDateTime::currentDateTime().toString(DATE_FORMAT)).arg(s);
    logWidget->insertPlainText(s1);
    if(logStream != NULL)
        *logStream << s1 << flush;
    emit scroll();
}

void Logger::Warning(QString s) {
    QString s1 = QString("[W] {%1} %2\n").arg(QDateTime::currentDateTime().toString(DATE_FORMAT)).arg(s);
    logWidget->insertPlainText(s1);
    if(logStream != NULL)
        *logStream << s1 << flush;
    emit scroll();
}

void Logger::Error(QString s) {
    QString s1 = QString("[E] {%1} %2\n").arg(QDateTime::currentDateTime().toString(DATE_FORMAT)).arg(s);
    logWidget->insertPlainText(s1);
    if(logStream != NULL)
        *logStream << s1 << flush;
    emit scroll();
}

/*
void Logger::Info_Hex(const uint8_t *pbtData, const size_t szBytes, bool newLine)
{
    size_t  szPos;
    QString out;

    for (szPos = 0; szPos < szBytes; szPos++) {
        QString tmp;
        tmp.sprintf("%02x  ", pbtData[szPos]);
        out.append(tmp);
    }
    if(newLine)
        out.append("\r\n");

    Info_noNL(out);
}
*/


void Logger::scrollToCaret() {
    QTextCursor c = logWidget->textCursor();
    c.movePosition(QTextCursor::End);
    logWidget->setTextCursor(c);
}
